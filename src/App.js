import { BrowserRouter } from "react-router-dom";
import Routers from "./routes/router";

function App() {
  return (
    <BrowserRouter>
      <Routers />
    </BrowserRouter>
  );
}

export default App;
