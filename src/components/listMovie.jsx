import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getMovie } from "../store/actions/movies";

const ListMovie = () => {
  const dispatch = useDispatch();
  const { data, loading } = useSelector((state) => state.movies);
  console.log("data dari reducer", data);
  console.log("loading dari reducer", loading);
  useEffect(() => {
    dispatch(getMovie());
  }, [dispatch]);
  return (
    <div>
      <h1>List Movie</h1>
      <ul>
        {data.map((item, index) => (
          <a href={`/movie/${item.id}`} key={index}>
            <div>
              <img
                src={item.picture}
                alt={item.title}
                style={{ maxWidth: "200px" }}
              />
              <h4>{item.title}</h4>
              <p>rating : {item.rating}</p>
            </div>
          </a>
        ))}
      </ul>
    </div>
  );
};

export default ListMovie;
