import { useState } from "react";
import { useDispatch } from "react-redux";
import { addMovie } from "../store/actions/movies";

const AddMovie = () => {
  const dispatch = useDispatch();
  const [newMovie, setNewMovie] = useState({
    title: "",
    rating: 0,
    picture: "",
    details: {
      year: 0,
      budget: 0,
      language: "",
    },
  });
  console.log(newMovie);
  const inputHandle = (e) => {
    setNewMovie({
      ...newMovie,
      [e.target.name]: e.target.value,
    });
  };
  const inputDetails = (e) => {
    setNewMovie({
      ...newMovie,
      details: { ...newMovie.details, [e.target.name]: e.target.value },
    });
  };
  const handlePicture = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      setNewMovie({
        ...newMovie,
        picture: reader.result,
      });
    };
  };
  const addNewMovie = () => {
    dispatch(addMovie(newMovie));
  };
  return (
    <div>
      <h1>Add Movie</h1>
      <label>Title</label>
      <input
        type="text"
        name="title"
        placeholder="title"
        onChange={(e) => inputHandle(e)}
      />
      <label>Year</label>
      <input type="number" name="year" onChange={(e) => inputDetails(e)} />
      <label>Budget</label>
      <input
        type="text"
        name="budget"
        placeholder="budget"
        onChange={(e) => inputDetails(e)}
      />
      <label>Language</label>
      <input
        type="text"
        name="language"
        placeholder="language"
        onChange={(e) => inputDetails(e)}
      />
      <label>Rating</label>
      <input type="number" name="rating" onChange={(e) => inputHandle(e)} />
      <label>Picture</label>
      <input type="file" name="picture" onChange={(e) => handlePicture(e)} />
      <button onClick={addNewMovie}>Submit</button>
    </div>
  );
};

export default AddMovie;
