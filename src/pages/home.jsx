import "../App.css";
import ListMovie from "../components/listMovie";

function Home() {
  // const plus = (num1, num2) => {
  //   return num1 + num2;
  // };
  return (
    <div className="App">
      <h1>New Movie App</h1>
      <a href="/add-movie">
        <p>Add Movie</p>
      </a>
      {/* <p>{plus(1, 2)}</p> */}
      <ListMovie />
    </div>
  );
}

export default Home;
