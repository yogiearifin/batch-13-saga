import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getMovieDetails, addReview } from "../store/actions/movies";
import { useParams } from "react-router";

const MovieDetails = () => {
  const dispatch = useDispatch();
  const { detail, loading } = useSelector((state) => state.movies.detailMovie);
  const [allDetails, setAllDetails] = useState({});
  console.log("all Details", allDetails);
  const { id } = useParams();
  const [review, setReviews] = useState({
    id: id,
    title: detail?.title,
    reviews: {
      review: "",
      rating: "",
    },
  });
  console.log("review", review);
  const handleReview = (e) => {
    setReviews({
      ...review,
      reviews: { ...review.reviews, [e.target.name]: e.target.value },
    });
  };
  console.log(id);
  useEffect(() => {
    dispatch(getMovieDetails(id));
  }, [dispatch, id]);

  useEffect(() => {
    setAllDetails(detail);
    setReviews({ ...review, title: detail?.title });
  }, [detail, review]);

  const submitReview = () => {
    dispatch(addReview(review));
  };
  return (
    <div>
      <h1>Details</h1>
      {loading ? (
        "Loading..."
      ) : detail && detail.details && detail.reviews ? (
        <div>
          <h1>{detail.title}</h1>
          <img src={detail.picture} alt={detail.title} />
          <p>Year: {detail.details.year}</p>
          <p>Budget: {detail.details.budget}</p>
          <p>Language: {detail.details.language}</p>
          <p>Rating: {detail.rating}</p>
          <div>
            <h1>Reviews</h1>
            {detail.reviews.map((item, index) => {
              return (
                <div key={index}>
                  <p>{item.review}</p>
                  <p>User Rating: {item.rating}</p>
                </div>
              );
            })}
          </div>
          <div>
            <h1>Write your own review!</h1>
            <label>Review</label>
            <input
              type="text"
              name="review"
              placeholder="review"
              onChange={(e) => handleReview(e)}
            />
            <input
              type="number"
              name="rating"
              onChange={(e) => handleReview(e)}
            />
            <button onClick={submitReview}>Submit Review</button>
          </div>
        </div>
      ) : (
        "Loading"
      )}
    </div>
  );
};

export default MovieDetails;
