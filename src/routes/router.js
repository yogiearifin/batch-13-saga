import { Switch, Route } from "react-router-dom";

import Home from "../pages/home";
import AddMovie from "../pages/addMovie";
import MovieDetails from "../pages/movieDetails";

const Routers = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/add-movie">
        <AddMovie />
      </Route>
      <Route exact path="/movie/:id">
        <MovieDetails />
      </Route>
      <Route path="*">
        <div>
          <h1>Not Found :(</h1>
        </div>
      </Route>
    </Switch>
  );
};

export default Routers;
