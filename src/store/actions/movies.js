import {
  GET_MOVIE_BEGIN,
  ADD_MOVIE_BEGIN,
  GET_MOVIE_DETAILS_BEGIN,
  ADD_REVIEW_BEGIN,
} from "./types";

export const getMovie = () => {
  return {
    type: GET_MOVIE_BEGIN,
  };
};

export const addMovie = (body) => {
  return {
    type: ADD_MOVIE_BEGIN,
    payload: body,
  };
};

export const getMovieDetails = (id) => {
  return {
    type: GET_MOVIE_DETAILS_BEGIN,
    id,
  };
};

export const addReview = (body) => {
  return {
    type: ADD_REVIEW_BEGIN,
    payload: body,
  };
};
