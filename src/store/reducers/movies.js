import {
  GET_MOVIE_BEGIN,
  GET_MOVIE_FAIL,
  GET_MOVIE_SUCCESS,
  GET_MOVIE_DETAILS_BEGIN,
  GET_MOVIE_DETAILS_SUCCESS,
  GET_MOVIE_DETAILS_FAIL,
} from "../actions/types";

const intialState = {
  data: [],
  loading: false,
  error: null,
  detailMovie: {
    detail: {},
    loading: false,
    error: null,
  },
};

const movies = (state = intialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    default:
      return {
        ...state,
      };
    case GET_MOVIE_BEGIN:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case GET_MOVIE_SUCCESS:
      return {
        ...state,
        data: payload,
        loading: false,
        error: null,
      };
    case GET_MOVIE_FAIL:
      return {
        data: [],
        loading: false,
        error: error,
      };
    case GET_MOVIE_DETAILS_BEGIN:
      return {
        ...state,
        detailMovie: {
          loading: true,
          error: false,
        },
      };
    case GET_MOVIE_DETAILS_SUCCESS:
      return {
        ...state,
        detailMovie: {
          detail: payload,
          loading: false,
          error: null,
        },
      };
    case GET_MOVIE_DETAILS_FAIL:
      return {
        ...state,
        detailMovie: {
          detail: {},
          loading: false,
          error: error,
        },
      };
  }
};

export default movies;
