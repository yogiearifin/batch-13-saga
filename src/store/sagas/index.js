import { all } from "redux-saga/effects";
import {
  watchGetMovies,
  watchAddMovie,
  watchGetDetailsMovie,
  watchAddReview,
} from "./movie";

export default function* rootSaga() {
  yield all([
    watchGetMovies(),
    watchAddMovie(),
    watchGetDetailsMovie(),
    watchAddReview(),
  ]);
}
