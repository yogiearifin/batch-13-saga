import { put, takeEvery } from "@redux-saga/core/effects";
import axios from "axios";
import {
  GET_MOVIE_BEGIN,
  GET_MOVIE_SUCCESS,
  GET_MOVIE_FAIL,
  ADD_MOVIE_BEGIN,
  ADD_MOVIE_SUCCESS,
  ADD_MOVIE_FAIL,
  GET_MOVIE_DETAILS_BEGIN,
  GET_MOVIE_DETAILS_SUCCESS,
  GET_MOVIE_DETAILS_FAIL,
  ADD_REVIEW_BEGIN,
  ADD_REVIEW_SUCCESS,
  ADD_REVIEW_FAIL,
} from "../actions/types";

const baseURl = "http://localhost:4000/movie";
const baseURLReview = "http://localhost:4000/review";

function* getMovies(actions) {
  const { error } = actions;
  try {
    const res = yield axios.get(baseURl);
    console.log(res);
    yield put({
      type: GET_MOVIE_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    yield put({
      type: GET_MOVIE_FAIL,
      error: error,
    });
    console.log(err);
  }
}

function* addMovie(actions) {
  const { payload, error } = actions;
  try {
    const res = yield axios.post(baseURl, payload);
    console.log(res);
    yield put({ type: ADD_MOVIE_SUCCESS });
  } catch (err) {
    yield put({
      type: ADD_MOVIE_FAIL,
      error: error,
    });
  }
}

function* getMovieDetails(actions) {
  const { error, id } = actions;
  try {
    const res = yield axios.get(`${baseURl}/${id}`);
    console.log(res);
    yield put({
      type: GET_MOVIE_DETAILS_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    yield put({
      type: GET_MOVIE_DETAILS_FAIL,
      error: error,
    });
  }
}

function* addReview(actions) {
  const { payload, error } = actions;
  try {
    const res = yield axios.post(baseURLReview, payload);
    console.log(res);
    yield put({
      type: ADD_REVIEW_SUCCESS,
    });
  } catch (err) {
    yield put({
      type: ADD_REVIEW_FAIL,
      error: error,
    });
  }
}

export function* watchGetMovies() {
  yield takeEvery(GET_MOVIE_BEGIN, getMovies);
}

export function* watchAddMovie() {
  yield takeEvery(ADD_MOVIE_BEGIN, addMovie);
}

export function* watchGetDetailsMovie() {
  yield takeEvery(GET_MOVIE_DETAILS_BEGIN, getMovieDetails);
}

export function* watchAddReview() {
  yield takeEvery(ADD_REVIEW_BEGIN, addReview);
}
